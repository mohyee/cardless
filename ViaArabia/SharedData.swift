//
//  SharedData.swift
//  cardless
//
//  Created by Mohyee Tamimi on 11/11/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import Foundation

private var sharedData: SharedData? = nil

open class SharedData {
    
    let userDefualts = UserDefaults.standard
    
    class var getInstance : SharedData {
        if sharedData == nil {
            sharedData = SharedData()
        }
        return sharedData!
    }
    
    func setLanguage(lang: String) {
        self.userDefualts.set(lang, forKey: Constants.DEVICE_LANGUAGE)
        self.userDefualts.synchronize()
    }
    
    func getLanguage() -> String? {
        let lang = self.userDefualts.value(forKey: Constants.DEVICE_LANGUAGE) as? String
        if lang == nil {
            let langStr = Locale.current.languageCode
            if langStr != "ar" || langStr != "en" {
                return "en"
            }
            return langStr
        }
        return lang
    }
    

    func setUserLoggedIn(userLoggedIn: Bool) {
        self.userDefualts.set(userLoggedIn, forKey: "user_logged_in")
        self.userDefualts.synchronize()
    }
    
    func isUserLoggedIn() -> Bool {
        let loggedIn = self.userDefualts.value(forKey: "user_logged_in") as? Bool
        if loggedIn == nil {
            return false
        }
        return self.userDefualts.value(forKey: "user_logged_in") as! Bool
    }
    
    func setFullName(fName: String) {
        self.userDefualts.set(fName, forKey: Constants.USER_NAME)
        self.userDefualts.synchronize()
    }
    
    func getFullName() -> String? {
        return self.userDefualts.value(forKey: Constants.USER_NAME) as? String
    }
    
    func setUserName(lName: String) {
        self.userDefualts.set(lName, forKey: Constants.USER_PHONE)
        self.userDefualts.synchronize()
    }
    
    func getUserName() -> String? {
        return self.userDefualts.value(forKey: Constants.USER_PHONE) as? String
    }
    
    func setBalance(email: String) {
        self.userDefualts.set(email, forKey: Constants.USER_CREDIT)
        self.userDefualts.synchronize()
    }
    
    func getBalance() -> String? {
        return self.userDefualts.value(forKey: Constants.USER_CREDIT) as? String
    }
    
    func setMobile(mobile: String) {
        self.userDefualts.set(mobile, forKey: "user_mobile")
        self.userDefualts.synchronize()
    }
    
    func getMobile() -> String? {
        return self.userDefualts.value(forKey: "user_mobile") as? String
    }
    
    func setRememberMe(flag: Bool) {
        self.userDefualts.set(flag, forKey: "remember_me")
        self.userDefualts.synchronize()
    }
    
    func getRememberMe() -> Bool {
        let rememberMe = self.userDefualts.value(forKey: "remember_me") as? Bool
        if rememberMe == nil {
            return false
        }
        return self.userDefualts.value(forKey: "remember_me") as! Bool
    }
    
    func setShowWalkThrough(flag: Bool) {
        self.userDefualts.set(flag, forKey: "walk_through")
        self.userDefualts.synchronize()
    }
    
    func getShowWalkThrough() -> Bool {
        let rememberMe = self.userDefualts.value(forKey: "walk_through") as? Bool
        if rememberMe == nil {
            return true
        }
        return self.userDefualts.value(forKey: "walk_through") as! Bool
    }
}

