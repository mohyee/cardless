//
//  MyOrdersTableViewCell.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/21/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var myOrdersImage: UIImageView!
    @IBOutlet weak var myOrdersTitle: UILabel!
    @IBOutlet weak var myOrdersSubTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.myOrdersImage.layer.cornerRadius = 3.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
