//
//  ProductTableViewCell.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.productImage.layer.cornerRadius = 3.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
