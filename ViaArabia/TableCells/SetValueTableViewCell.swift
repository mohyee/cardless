//
//  SetValueTableViewCell.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class SetValueTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var notAvailableLabel: UILabel!
    @IBOutlet weak var notAvailableView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.notAvailableView.layer.cornerRadius = 20.0
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
