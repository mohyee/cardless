//
//  HomeTabBarController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    @IBOutlet weak var tBar: UITabBar!
    let language = UserDefaults.standard.string(forKey: Constants.DEVICE_LANGUAGE)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "CARDLESS"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)


        if self.tabBarController != nil {
            self.tabBarController?.selectedIndex = 2
        }
        
        if language == "ar" {
            
            let productsTab: UITabBarItem = self.tabBar.items![3] as UITabBarItem
            productsTab.title = "المنتجات"
            
            let myOrdersTab: UITabBarItem = self.tabBar.items![2] as UITabBarItem
            myOrdersTab.title = "مشترياتي"
            
            let supportTab: UITabBarItem = self.tabBar.items![1] as UITabBarItem
            supportTab.title = "الدعم"
            
            let moreTab: UITabBarItem = self.tabBar.items![0] as UITabBarItem
            moreTab.title = "المزيد"
            
            self.selectedIndex = 3
        }
        self.delegate = self


        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
