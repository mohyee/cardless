//
//  MoreViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 9/29/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var logoutButton: UIButton!
    
    lazy var language = SharedData.getInstance.getLanguage()
    let sharedData = SharedData.getInstance

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController!.topViewController!.navigationItem.title = "More"

        if language == "ar" {
            self.navigationController!.topViewController!.navigationItem.title = "المزيد"
            self.tableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.logoutButton.setTitle("خروج", for: UIControlState.normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate   = self
        self.tableView.dataSource = self
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        self.presentConfirmationAlertView()
    }
    
    
    func presentConfirmationAlertView() {
        
        let message = (self.language == "ar") ? "هل تريد تسجيل الخروج ؟ " : "Are you sure you want to logout ?"
        let yesString = (self.language == "ar") ? "نعم" : "Yes"
        let noString  = (self.language == "ar") ? "لا" : "No"
        
        let alert = UIAlertController(title: nil , message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let option1 = UIAlertAction(title: yesString, style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            //yes logut
            UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showLogin(isFirstTime: false)
            
//            self.navigationController?.popViewController(animated: true)
            
        }
        alert.addAction(option1)
        
        let option2 = UIAlertAction(title: noString, style: UIAlertActionStyle.cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(option2)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as UITableViewCell!
        if !(cell != nil) {
            cell = UITableViewCell(style:.default, reuseIdentifier: "CELL")
        }
        
        
        if indexPath.row == 0 {
            cell!.textLabel?.text = (self.language == "ar") ? "حسابي" : "My Profile"
        } else if indexPath.row == 1 {
            cell!.textLabel?.text = (self.language == "ar") ? "تواصل معنا" : "Contact Us"
        } else if indexPath.row == 2 {
            cell!.textLabel?.text = (self.language == "ar") ? "الشروط والأحكام" : "Terms & Conditions"
        }   else if indexPath.row == 3 {
            cell!.textLabel?.text = (self.language == "ar") ? "Change Language" : "تغيير اللغة"
        }
        
        if language == "ar" {
            cell!.textLabel?.transform     = CGAffineTransform(scaleX: -1, y: 1)
            cell!.textLabel?.textAlignment = NSTextAlignment.right
        }
        
        cell!.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: Constants.SEGUE_MY_PROFILE, sender: self)
        } else if indexPath.row == 1 {
            self.performSegue(withIdentifier: Constants.SEGUE_CONTACT_US, sender: self)
        } else if indexPath.row == 2 {
            self.performSegue(withIdentifier: Constants.SEGUE_TERMS, sender: self)
        } else if indexPath.row == 3 {
            self.changeLanguage()
        }
    }
    
    func changeLanguage() {
        
        var message = "Change Language?"
        var cancelString = "Cancel"
        
        if self.language != "ar" {
            message      = "تغيير اللغة ؟"
            cancelString = "إالغاء"
        }
        
        print("Choose Language Action")
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action1 = UIAlertAction(title: "English", style: UIAlertActionStyle.default, handler: { (action) in
            
            if self.language == "en" {
                return
            }
            self.sharedData.setLanguage(lang: "en")
            self.performSegue(withIdentifier: Constants.SEGUE_CHANGE_LANGUAGE, sender: self)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showMainTab(isFirstTime: false)
            
//            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(action1)
        
        let action2 = UIAlertAction(title: "عربي", style: UIAlertActionStyle.default, handler: { (action) in
            if self.language == "ar" {
                return
            }
            self.sharedData.setLanguage(lang: "ar")
            self.performSegue(withIdentifier: Constants.SEGUE_CHANGE_LANGUAGE, sender: self)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showMainTab(isFirstTime: false)
            
//            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(action2)
        
        let action3 = UIAlertAction(title: cancelString, style: UIAlertActionStyle.cancel, handler: { (action) in
        })
        alert.addAction(action3)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using
        
        if segue.identifier == Constants.SEGUE_MY_PROFILE {
            //let myProfieViewController  = segue.destination as! MyProfileViewController
        } else if segue.identifier == Constants.SEGUE_CONTACT_US {
            //let contactUsViewController = segue.destination as! ContactUsViewController
        } else if segue.identifier == Constants.SEGUE_TERMS {
            //let termsViewController = segue.destination as! TermsViewController
        } else if segue.identifier == Constants.SEGUE_CHANGE_LANGUAGE {
            print("SETTING LANGUAGE...")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

