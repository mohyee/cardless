//
//  PaymentViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var howManyCardsLabel: UILabel!
    @IBOutlet var addQuantityButton: UIButton!
    @IBOutlet var deductQuantityButton: UIButton!
    @IBOutlet var quantityTextfield: UITextField!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var totalAmountPriceLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var balancePriceLabel: UILabel!
    @IBOutlet var totalAmountView: UIView!
    @IBOutlet var balanceView: UIView!
    @IBOutlet var checkOutBalanceButton: UIButton!
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet var quantityView: UIView!

    lazy var language = SharedData.getInstance.getLanguage()

    var quantityValue: Int = 1
    var productPriceValue: Int = 0
    var productObject: ProductObject?
    var createObject: CreateOrderObject!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Payment"
        self.tabBarController?.tabBar.isHidden = true

        self.productImage.layer.cornerRadius = 5.0
        self.quantityView.layer.cornerRadius = 5.0
        self.quantityView.layer.borderColor  = Colors.colorFromHex(0xababab).cgColor
        self.quantityView.layer.borderWidth  = 0.6
        
        self.quantityView.backgroundColor = Colors.colorFromHex(0xf7f7f7)
        
        self.howManyCardsLabel.text = "How Many Card You Want to Buy ?"
        self.totalAmountLabel.text  = "TOTAL AMOUNT"
        self.balanceLabel.text      = "BALANCE"
        self.checkOutBalanceButton.setTitle("CHECK OUT", for: UIControlState.normal)

        if language == "ar" {
            self.title = "الدفع"
            self.howManyCardsLabel.text = "حدد الكمية التي تريد ؟"
            self.totalAmountLabel.text  = "المجموع"
            self.balanceLabel.text      = "الرصيد"
            self.checkOutBalanceButton.setTitle("إنتهاء", for: UIControlState.normal)
        }
        
        if productObject != nil {
            
            let userCredit: String  = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!
            self.balancePriceLabel.attributedText = Util.subscriptText(creditString: userCredit, subStringSize: 17)

            self.totalAmountPriceLabel.attributedText = Util.subscriptText(creditString: "\(productObject!.price)", subStringSize: 17)
            
            self.productNameLabel.text  = productObject!.skuName
            
            self.productPriceLabel.attributedText = Util.subscriptText(creditString: "\(productObject!.price)", subStringSize: 17)
            
            if let url = URL(string: productObject!.productimgurl) {
                self.downloadImage(url: url, imageView: self.productImage)
            }
            self.productPriceValue = Int(productObject!.price)
        }
        
        self.quantityTextfield.text = "\(quantityValue)"
        self.quantityTextfield.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func addQuantityAction(_ sender: Any) {
        
        print("add Quantity Action")
        self.quantityValue += 1
        self.quantityTextfield.text = "\(quantityValue)"
        
        let totalAmount: Int = self.quantityValue * self.productPriceValue
//        totalAmountPriceLabel.text = "\(totalAmount) SAR"
        
        self.totalAmountPriceLabel.attributedText = Util.subscriptText(creditString: "\(totalAmount)", subStringSize: 17)
    }
    
    @IBAction func deductQuantityAction(_ sender: Any) {
        print("deduct Quantity Action")
        
        if self.quantityValue <= 1 {
            self.quantityValue = 1
        } else {
            self.quantityValue -= 1
        }
        
        self.quantityTextfield.text = "\(quantityValue)"
        
        let totalAmount: Int = self.quantityValue * self.productPriceValue
        
        self.totalAmountPriceLabel.attributedText = Util.subscriptText(creditString: "\(totalAmount)", subStringSize: 17)
    }
    
    @IBAction func checkOutAction(_ sender: Any) {
        self.createOrderAPI()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (textField.text?.isNumber)! {
            self.quantityValue = Int(self.quantityTextfield.text!)!
            
            let totalAmount: Int = self.quantityValue * self.productPriceValue
            
            self.totalAmountPriceLabel.attributedText = Util.subscriptText(creditString: "\(totalAmount)", subStringSize: 17)
        }
        return false
    }
    
    func presentConfirmationAlertView() {
        
        let message   = (self.language == "ar") ? "هل تريد إكمال عملية الشراء ؟" : "Are you sure ?"
        
        let yesString = (self.language == "ar") ? "نعم" : "Yes"
        let noString  = (self.language == "ar") ? "لا" : "No"

        let alert = UIAlertController(title: nil , message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let option1 = UIAlertAction(title: yesString, style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.confirmOrderAPI()
        }
        alert.addAction(option1)
        
        let option2 = UIAlertAction(title: noString, style: UIAlertActionStyle.cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(option2)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showOrderDetails(itemDetail: ItemDetailsObject) {
        let orderDetails = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderDetails") as! OrderDetailsViewController
        orderDetails.isPresentActive = true
        orderDetails.itemDetailsObjects = [itemDetail]
        
        let navViewController: UINavigationController = UINavigationController(rootViewController: orderDetails)
        
        self.present(navViewController, animated: true) {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: false)
        }
    }
    
    //API Call
    func createOrderAPI() {
        
        let message   = (self.language == "ar") ? "يرجى ادخال الكمية بشكل صحيح" : "Please Enter a Valid Quanitiy "
        
        let okString   = (self.language == "ar") ? "موافق" : "OK"
        
        if self.quantityTextfield.text!.isNumber {
            self.quantityValue = Int(self.quantityTextfield.text!)!
        } else {
            Util.showInfoAlert(msg: message, buttonTitle: okString, controller: self, callback: { (success) in
                self.quantityTextfield.becomeFirstResponder()
            })
            return
        }

        let token: String = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = [
            "method": Constants.METHOD_CREATE_ORDER,
            "itemdet": [
                [
                    "sku": productObject!.sku,
                    "qty": "\(self.quantityValue)"
                ]
            ]
            ] as [String : Any]
        
        self.checkOutBalanceButton.isHidden = true
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_CREATE_ORDER, parameters: params, headers: headers) { (data) in
            if data != nil {
                print(data!)
                self.loading.stopAnimating()
                self.checkOutBalanceButton.isHidden = false
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                if statusValue == 0 {
                    self.createObject = CreateOrderObject(createResponse: results[0])
                    self.presentConfirmationAlertView()
                    
                } else if statusValue == 206 {
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLogin(isFirstTime: false)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                    })
                    return
                    
                } else {
                    self.loading.stopAnimating()
                    self.checkOutBalanceButton.isHidden = false
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                    })
                    return
                }
                
            } else {
                self.loading.stopAnimating()
                self.checkOutBalanceButton.isHidden = false
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }
    
    func confirmOrderAPI()  {
        
        let token: String = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = ["method"       : Constants.METHOD_CONFIRM_ORDER,
                      "transactionid": self.createObject.transactionid,
                      "paymenttype"  : "CC",
                      "paymentref"   : "Pay123456"]
        
        self.checkOutBalanceButton.isHidden = true
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_CONFIRM_ORDER, parameters: params, headers: headers) { (data) in
            if data != nil {
                print(data!)
                self.checkOutBalanceButton.isHidden = false
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                if statusValue == 0 {
                    
                    let newCredit = status["balance"].intValue
                    UserDefaults.standard.set(String(newCredit), forKey: Constants.USER_CREDIT)
                    
                    let newPoints = status["points"].intValue
                    UserDefaults.standard.set(String(newPoints), forKey: Constants.USER_POINTS)

                    let message   = (self.language == "ar") ? "تم الشراء بنجاح" : "Your Order was processed successfully"
                    let okString   = (self.language == "ar") ? "موافق" : "OK"
                    
                    let temp = status["itemdet"].arrayValue
                    let itemDetail = ItemDetailsObject(itemDetails: temp[0])
                    itemDetail.transactionId  = status["transactionid"].stringValue
                    itemDetail.categoryImgUrl = (self.productObject!.productimgurl)
                    Util.showInfoAlert(msg: message, buttonTitle: okString, controller: self, callback: { (success) in
                        self.showOrderDetails(itemDetail: itemDetail)
                    })
                    
                } else if statusValue == 206 {
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                    })
                    return
                } else {
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                    })
                    return
                }
                
            } else {
                self.loading.stopAnimating()
                self.checkOutBalanceButton.isHidden = false
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }
    
    //Getting Image from URL
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

