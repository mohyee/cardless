//
//  ProductsViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet weak var availableCreditLabel: UILabel!

    @IBOutlet var searchBar: UISearchBar!
    
    lazy var language = SharedData.getInstance.getLanguage()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var categoryObjects: [CategoryObject]!
    var categoryObjectsFiltered: [CategoryObject]!
    var categoryObjectsOriginal: [CategoryObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.topViewController!.navigationItem.title = "CARDLESS"
        
        if language == "ar" {
            self.tableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.searchBar.placeholder = "بحث"
            self.title = "المنتجات"
        }
        
        self.searchBar.delegate = self

        self.tableView.isHidden   = true
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "productCell")
        
        let userCredit: String  = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!
        self.availableCreditLabel.attributedText =  Util.balanceSubscriptText(creditString: userCredit, subStringSize: 12.5)
        
         self.categoryAPICall()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
        if searchText.count > 0 {
            self.categoryObjectsFiltered = self.categoryObjects
            var newArrData = [CategoryObject]()

            for item in self.categoryObjectsFiltered {
                if item.categoryName.contains(searchText) {
                    newArrData.append(item)
                }
            }
            self.categoryObjectsFiltered = newArrData
            self.categoryObjects = self.categoryObjectsFiltered
            self.tableView.reloadData()
            
        } else {
            self.categoryObjects = self.categoryObjectsOriginal
            self.tableView.reloadData()
        }
        
    }
    
    func categoryAPICall() {
        
        self.categoryObjects = [CategoryObject]()
        let token: String    = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = ["method":  Constants.METHOD_LIST_CATEGORY]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_LIST_CATEGORY, parameters: params, headers: headers) { (data) in
            if data != nil {
                print(data!)
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                if statusValue == 0 {
                    
                    for temp in status["catdetails"].arrayValue {
                        let categoryObject = CategoryObject(category: temp)
                        self.categoryObjects.append(categoryObject)
                    }
                    self.categoryObjectsOriginal = self.categoryObjects
                    
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    
                } else if (statusValue == 206 || statusValue == 201) {
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLogin(isFirstTime: false)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                    })
                    return
                } else {
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                    })
                    return
                }
                
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryObjects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell") as? ProductTableViewCell
        
        if cell == nil {
            let nib: [Any] = Bundle.main.loadNibNamed("ProductTableViewCell", owner: self, options: nil)!
            cell = nib[0] as? ProductTableViewCell
        }
        
        let categoryObject = self.categoryObjects[indexPath.row]
        
        cell!.productTitle.text  = categoryObject.categoryName
        
        if let url = URL(string: categoryObject.imageUrl) {
            self.downloadImage(url: url, imageView: cell!.productImage)
        }
        
        if language == "ar" {
            cell!.productTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.productImage.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.productTitle.textAlignment = NSTextAlignment.right
        }
        
        cell!.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        let categoryName = self.categoryObjects[indexPath.row].categoryType
        self.performSegue(withIdentifier: Constants.SEGUE_SET_VALUE, sender: categoryName)
    }
    
    
    //Getting Image from URL
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using
        
        if segue.identifier == Constants.SEGUE_SET_VALUE {
            let setValueController = segue.destination as! SetValueViewController
            
            let categoryName =  sender as? String ?? ""
            setValueController.category = categoryName
        }
        // Pass the selected object to the new view controller.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

