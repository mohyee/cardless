//
//  OrderPaymentViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/18/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class OrderPaymentViewController: UIViewController {

    @IBOutlet var nameAndCostView: UIView!
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productNameLabel: UILabel!
    
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var redeemView: UIView!
    @IBOutlet var serialView: UIView!
    @IBOutlet var dateView: UIView!
    
    @IBOutlet var redeemCodeLabel: UILabel!
    @IBOutlet var serialNumberLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var redeemCodeValueLabel: UILabel!
    @IBOutlet var serialNumberValueLabel: UILabel!
    @IBOutlet var dateValueLabel: UILabel!

    lazy var language = SharedData.getInstance.getLanguage()

    var itemDetailsObject: ItemDetailsObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Payment"
        
        self.redeemCodeLabel.text   = "REDEEM CODE"
        self.serialNumberLabel.text = "SERIAL NUMBER"
        self.dateLabel.text         = "BOUGHT ON"
        
        if language == "ar" {
            self.title = "الدفع"
            
            self.nameAndCostView.transform   = CGAffineTransform(scaleX: -1, y: 1)
            self.productNameLabel.transform  = CGAffineTransform(scaleX: -1, y: 1)
            self.productPriceLabel.transform = CGAffineTransform(scaleX: -1, y: 1)

            
            self.redeemCodeLabel.text   = "الكود"
            self.serialNumberLabel.text = "رمز التسلسل"
            self.dateLabel.text         = "تاريخ الشراء"
        }
        
        let shareButton = UIBarButtonItem(image: UIImage(named: "shareButton"), style: .plain, target: self, action: #selector(OrderPaymentViewController.shareAction(sender:))) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = shareButton

        if itemDetailsObject != nil {
            self.fillData()
        }
    }
    
    func fillData() {
        
        if let url = URL(string: itemDetailsObject!.productImgUrl) {
            self.downloadImage(url: url, imageView: self.productImage)
        }
        let dateTrimmed = self.itemDetailsObject!.orderdate.components(separatedBy: "T")
        
        self.productNameLabel.text     = self.itemDetailsObject!.skuName
        self.productPriceLabel.attributedText    = Util.subscriptText(creditString: String(self.itemDetailsObject!.price), subStringSize: 8)
        
        self.redeemCodeValueLabel.text   = self.itemDetailsObject!.code
        self.serialNumberValueLabel.text = self.itemDetailsObject!.serial
        self.dateValueLabel.text         = dateTrimmed[0]
    }

    func shareAction(sender: UIBarButtonItem) {
        
        print("Share Action Pressed")
        
        let codeString:String = itemDetailsObject!.code
        
        let textToShare = [ codeString ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        //        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        self.present(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler = {(activityType, success, items, error) in
            if success {
                print("success")
            }
        }
    }
    
    //Getting Image from URL
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
