//
//  TermsViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 9/29/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {

    @IBOutlet var webView: UIWebView!

    lazy var language = SharedData.getInstance.getLanguage()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Terms & Conditions"
        self.tabBarController?.tabBar.isHidden = true

        if language == "ar" {
            self.title = "الشروط والأحكام"
            
            let htmlFile = Bundle.main.path(forResource: "termsAR", ofType: "html")
            let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
            self.webView.loadHTMLString(html!, baseURL: nil)
        } else {
            let htmlFile = Bundle.main.path(forResource: "terms", ofType: "html")
            let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
            self.webView.loadHTMLString(html!, baseURL: nil)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
