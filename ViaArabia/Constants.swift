//
//  Constants.swift
//  JarirBookstore
//
//  Created by Amer Smadi on 8/14/16.
//  Copyright © 2016 Amer Smadi. All rights reserved.
//

import Foundation

open class Constants {
    
    // MARK: SEGUES
    
    static let SEGUE_LOGIN: String                  = "loginSegue"
    static let SEGUE_LOGIN_AR: String               = "loginArabic"
    static let SEGUE_SET_VALUE: String              = "setValueSegue"
    static let SEGUE_PAYMENT: String                = "paymentSegue"
    static let SEGUE_ORDER_DETAILS: String          = "orderDetailsSegue"
    static let SEGUE_ORDER_DETAILS_PAYMENT: String  = "orderDetailsToPaymentSegue"
    
    static let SEGUE_MY_PROFILE: String       = "myProfileSegue"
    static let SEGUE_CONTACT_US: String       = "contactUsSegue"
    static let SEGUE_TERMS: String            = "termsSegue"
    static let SEGUE_CHANGE_LANGUAGE: String  = "changeLanguageSegue"
    // MARK: SERVICES
    
    static let CARDLESS_BASE_URL: String      = "https://cardless.viaarabia.com"
    
    static let URL_LOGIN: String              = CARDLESS_BASE_URL + "/api/login/LoginAccount"
    static let URL_LIST_CATEGORY: String      = CARDLESS_BASE_URL + "/api/Product/ListCategory"
    static let URL_LIST_PRODUCT: String       = CARDLESS_BASE_URL + "/api/Product/ListProduct"
    static let URL_CREATE_ORDER: String       = CARDLESS_BASE_URL + "/api/order/CreateOrder"
    static let URL_CONFIRM_ORDER: String      = CARDLESS_BASE_URL + "/api/order/ConfirmOrder"
    static let URL_ORDER_HISTORY: String      = CARDLESS_BASE_URL + "/api/order/OrderHistory"
    static let URL_ORDER_HISTORY_DETAILS: String    = CARDLESS_BASE_URL + "/api/order/OrderHistoryDetails"
    
    // MARK: APIs METHODS
    
    static let METHOD_LOGIN: String                 = "login"
    static let METHOD_LIST_CATEGORY: String         = "ListCategory"
    static let METHOD_LIST_PRODUCT: String          = "ListProduct"
    static let METHOD_CREATE_ORDER: String          = "CreateOrder"
    static let METHOD_CONFIRM_ORDER: String         = "ConfirmOrder"
    static let METHOD_ORDER_HISTORY: String         = "OrderHistory"
    static let METHOD_ORDER_HISTORY_DETAILS: String = "OrderHistoryDetails"

    // MARK: USER DEFAULTS KEYS
    
    static let DEVICE_TOKEN: String        = "GCM_TOKEN"
    static let USER_TOKEN: String          = "token"
    static let USER_NAME: String           = "userName"
    static let USER_PHONE: String          = "userPhone"
    static let DEVICE_LANGUAGE: String     = "appLanguage"
    static let USER_CREDIT: String         = "availablecredit"
    static let CONTENT_TYPE: String        = "application/json"
    static let USER_POINTS: String         = "userPoints"
    
    // MARK: SERVICES PARAMS
    
    static let PARAM_CONTENT_TYPE: String          = "Content-Type"
    static let PARAM_API_USER_NAME: String         = "apiusername"
    static let PARAM_API_PASSWORD: String          = "apipassword"
    static let PARAM_API_REFERENCE_NUMBER: String  = "referencenumber"
    static let PARAM_API_LANG_CODE: String         = "lang_code"
    static let PARAM_API_TOKEN: String             = "token"
    
    static let PARAM_API_USER_NAME_VALUE: String   = "cardless_mobileapp"
    static let PARAM_API_PASSWORD_VALUE : String   = "%~?#q8!0t+v6*@8ft:?qiz(3kb+!1c"
    
    //MARK: MSGS
    
    static let MSG_WORNG: String = "Something went wrong, check connection."
    let CONS_TEST: String = "Something went wrong, check connection."

}
