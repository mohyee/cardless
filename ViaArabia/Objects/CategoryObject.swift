//
//  CategoryObject.swift
//  cardless
//
//  Created by Mohyee Tamimi on 10/14/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryObject: NSObject {
    
//    "category": "Apple iTun",
//    "categoryname": "Apple iTunes",
//    "image_url": "https://portal.viaarabia.com/img/via1.png",
//    "qty": 10317
    
    var categoryType    = String()
    var categoryName    = String()
    var imageUrl        = String()
    var qty             = Double()
    
    override init() { }
    
    init(category:JSON) {
        super.init()
        
        categoryType   = category["category"].stringValue
        categoryName   = category["categoryname"].stringValue
        imageUrl       = category["image_url"].stringValue
        qty            = category["qty"].doubleValue
        
    }

}
