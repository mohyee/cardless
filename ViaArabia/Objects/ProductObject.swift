//
//  ProductObject.swift
//  cardless
//
//  Created by Mohyee Tamimi on 10/14/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON


class ProductObject: NSObject {

//    "Category": "Apple iTun",
//    "SKU": "GC000005",
//    "SKUName": "Itunes Gift Card 100 $ Digital Voucher",
//    "Qty": 2429,
//    "Cost": 400,
//    "Price": 449,
//    "productimgurl": "https://portal.viaarabia.com/img/via1.png"
    
    var categoryType    = String()
    var sku             = String()
    var skuName         = String()
    var qty             = Double()
    var cost            = Double()
    var price           = Double()
    var productimgurl   = String()

    override init() { }
    
    init(product:JSON) {
        super.init()
        
        categoryType    = product["Category"].stringValue
        sku             = product["SKU"].stringValue
        skuName         = product["SKUName"].stringValue
        qty             = product["Qty"].doubleValue
        cost            = product["Cost"].doubleValue
        price           = product["Price"].doubleValue
        productimgurl   = product["productimgurl"].stringValue

    }
    
}
