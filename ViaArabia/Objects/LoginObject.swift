//
//  LoginObject.swift
//  cardless
//
//  Created by Mohyee Tamimi on 10/20/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginObject: NSObject {
    
    //    "referencenumber": "DBBBC539-5C5C-4BA6-88AB-AF6156633F26",
    //    "transactionid": "GC-00000000000561",
    //    "status": 0,
    //    "statusdiscription": "Success",
    //    "usertype": "1",
    //    "token": "DBBBC539-5C5C-4BA6-88AB-AF6156633F26",
    //    "fullname": "Qaiser",
    //    "phonenumber": "0549552176",
    //    "availablecredit": 500000
    
    var referencenumber     = String()
    var transactionid       = String()
    var status              = Int()
    var statusdiscription   = String()
    var usertype            = String()
    var token               = String()
    var fullname            = String()
    var phonenumber         = String()
    var availablecredit     = Double()
    var points              = String()
    
    override init() { }
    
    init(login:JSON) {
        super.init()
        
        referencenumber       = login["referencenumber"].stringValue
        transactionid         = login["transactionid"].stringValue
        status                = login["status"].intValue
        statusdiscription     = login["statusdiscription"].stringValue
        usertype              = login["usertype"].stringValue
        token                 = login["token"].stringValue
        fullname              = login["fullname"].stringValue
        phonenumber           = login["phonenumber"].stringValue
        availablecredit       = login["availablecredit"].doubleValue
        points                = login["points"].stringValue
    }
}

