//
//  CreateOrderObject.swift
//  cardless
//
//  Created by Mohyee Tamimi on 10/28/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class CreateOrderObject: NSObject {

    //expirydatetime = "2017-10-28T23:31:09.413Z";
    //referencenumber = 123456;
    //status = 0;
    //statusdiscription = Success;
    //transactionid = "GC-00000000003848";
    
    var expirydatetime     = String()
    var referencenumber    = Double()
    var status             = Int()
    var statusdiscription  = String()
    var transactionid      = String()

    override init() { }
    
    init(createResponse:JSON) {
        super.init()
        
        expirydatetime        = createResponse["expirydatetime"].stringValue
        referencenumber       = createResponse["referencenumber"].doubleValue
        status                = createResponse["status"].intValue
        statusdiscription     = createResponse["statusdiscription"].stringValue
        transactionid         = createResponse["transactionid"].stringValue
    }
}
