//
//  ItemDetailsObject.swift
//  cardless
//
//  Created by Mohyee Tamimi on 10/14/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ItemDetailsObject: NSObject {

//    "transactionid": "GC-00000000000215",
//    "sku": "GC000002",
//    "skuname": "Itunes Gift Card 15 $ Digital Voucher",
//    "price": 79,
//    "cost": 70,
//    "serial": "SB7924131XDK295DCP9",
//    "code": null,
//    "productimgurl": "https://portal.viaarabia.com/img/via1.png",
//    "categoryimgurl": "https://portal.viaarabia.com/img/via1.png"
    
    
    
    
    var transactionId   = String()
    var sku             = String()
    var skuName         = String()
    var price           = Double()
    var cost            = Double()
    var orderdate       = String()
    var serial          = String()
    var code            = String()
    var productImgUrl   = String()
    var categoryImgUrl  = String()
    
    override init() { }
    
    init(itemDetails:JSON) {
        super.init()
        
        transactionId    = itemDetails["transactionid"].stringValue
        sku              = itemDetails["sku"].stringValue
        skuName          = itemDetails["skuname"].stringValue
        price            = itemDetails["price"].doubleValue
        cost             = itemDetails["cost"].doubleValue
        orderdate        = itemDetails["orderdate"].stringValue
        serial           = itemDetails["serial"].stringValue
        code             = itemDetails["code"].stringValue
        productImgUrl    = itemDetails["productimgurl"].stringValue
        categoryImgUrl   = itemDetails["categoryimgurl"].stringValue

//        code = SK2415364KKMP9WF196;
//        cost = 57;
//        price = 62;
//        serial = SB7924131XDKM8MDCP7;
//        sku = GC000006;
//        skuname = "Google Play Gift Card - 10$";
    }
    
}
