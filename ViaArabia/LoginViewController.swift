//
//  LoginViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 03/09/2017.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var cardlessLogoImage: UIImageView!
    @IBOutlet var textFieldsView: UIView!
    @IBOutlet var mobileNumberTextfield: UITextField!
    @IBOutlet var fieldSeparatorView: UIView!
    @IBOutlet var passwordViewField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var loading: UIActivityIndicatorView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var language: String?
    var loginSegue = Constants.SEGUE_LOGIN
    let sharedData = SharedData.getInstance

    override func viewWillAppear(_ animated: Bool) {
        
        self.language = self.sharedData.getLanguage()

        self.title = "Log In"
        self.loginButton.setTitle("LOGIN", for: UIControlState.normal)
        self.mobileNumberTextfield.placeholder = "MOBILE NUMBER"
        self.passwordViewField.placeholder     = "PASSWORD"
        self.loginSegue = Constants.SEGUE_LOGIN

        if language == "ar" {
            self.title = "الدخول"
            self.loginButton.setTitle("دخول", for: UIControlState.normal)
            self.mobileNumberTextfield.placeholder = "رقم الهاتف"
            self.passwordViewField.placeholder     = "كلمة السر"
            self.loginSegue = Constants.SEGUE_LOGIN_AR
        }
        
//        if UserDefaults.standard.string(forKey: Constants.USER_TOKEN) != nil {
//
//            let token: String  = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
//            if token != "" {
//                self.performSegue(withIdentifier: self.loginSegue, sender: self)
//            }
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.passwordViewField.delegate = self
        
        self.loading.isHidden = true

    }

    @IBAction func loginAction(_ sender: Any) {
        self.loginAPICall()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == -20 {
            self.view.endEditing(true)
            self.loginAPICall()
        }
        return false
    }
    
    func loginAPICall() {
        
        if self.mobileNumberTextfield.text == "" {
            Util.showInfoAlert(msg: "Please enter Mobile Number", buttonTitle: "OK", controller: self, callback: { (success) in
            })
            return
        }
        
        if self.passwordViewField.text == "" {
            Util.showInfoAlert(msg: "Please enter Password", buttonTitle: "OK", controller: self, callback: { (success) in
            })
            return
        }
        

        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en"]
        
        let params = ["method":       Constants.METHOD_LOGIN,
                      "phonenumber":  self.mobileNumberTextfield.text!, //"0549552176"
                      "userpassword": self.passwordViewField.text!] //"qjamal"
        
        self.loading.isHidden = false
        self.loading.startAnimating()

        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_LOGIN, parameters: params, headers: headers) { (data) in
            if data != nil {
                print(data!)
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue

                if statusValue == 0 {
                    
                    let loginObject = LoginObject(login: results[0])
                    
                    UserDefaults.standard.set(loginObject.token, forKey: Constants.USER_TOKEN)
                    UserDefaults.standard.set(loginObject.availablecredit, forKey: Constants.USER_CREDIT)
                    UserDefaults.standard.set(loginObject.fullname, forKey: Constants.USER_NAME)
                    UserDefaults.standard.set(loginObject.phonenumber, forKey: Constants.USER_PHONE)
                    UserDefaults.standard.set(loginObject.points, forKey: Constants.USER_POINTS)
                    
//                    self.performSegue(withIdentifier: self.loginSegue, sender: self)
                    self.appDelegate.showMainTab(isFirstTime: false)
                    
                } else if statusValue == 201 {
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                        self.passwordViewField.text = ""
                    })
                    return
                } else {
                    
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                        self.passwordViewField.text = ""
                    })
                    return
                    
                }
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using
        
        if segue.identifier == self.loginSegue {
            let homeTabBarController = segue.destination as! HomeTabBarController
        }
        // Pass the selected object to the new view controller.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

