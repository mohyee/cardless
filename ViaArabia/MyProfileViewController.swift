//
//  MyProfileViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 9/29/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit


class MyProfileViewController: UIViewController {
    
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userNameValueLabel: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var fullNameValueLabel: UILabel!
    @IBOutlet var currentBalanceLabel: UILabel!
    @IBOutlet var currentBalanceValueLabel: UILabel!
    
    @IBOutlet var currentPointLabel: UILabel!
    @IBOutlet var currentPointValueLabel: UILabel!
    
    
    lazy var language = SharedData.getInstance.getLanguage()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Profile"
        self.tabBarController?.tabBar.isHidden = true
        
        self.userNameLabel.text       = "USER NAME"
        self.fullNameLabel.text       = "FULL NAME"
        self.currentBalanceLabel.text = "CURRENT BALANCE"
        self.currentPointLabel.text   = "POINTS"
        
        if language == "ar" {
            self.title = "حسابي"
            
            self.userNameLabel.text       = "اسم المستخدم"
            self.fullNameLabel.text       = "الإسم الكامل"
            self.currentBalanceLabel.text = "رصيد الحالي"
            self.currentPointLabel.text   = "النقاط"
            
        }
        
        let userCredit = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!

        let userName: String    = UserDefaults.standard.string(forKey: Constants.USER_NAME)!
        let userPhone: String    = UserDefaults.standard.string(forKey: Constants.USER_PHONE)!
        let userPoints: String = UserDefaults.standard.string(forKey: Constants.USER_POINTS)!
        
        self.userNameValueLabel.text = userPhone
        self.fullNameValueLabel.text = userName
        self.currentBalanceValueLabel.attributedText = Util.subscriptText(creditString: userCredit, subStringSize: 13)
        self.currentPointValueLabel.text = userPoints
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
