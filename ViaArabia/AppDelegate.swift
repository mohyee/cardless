//
//  AppDelegate.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 03/09/2017.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let sharedData = SharedData.getInstance

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let appLanguage = self.sharedData.getLanguage()
        self.sharedData.setLanguage(lang: appLanguage!)
        
        if UserDefaults.standard.string(forKey: Constants.USER_TOKEN) == nil {
            self.showLogin(isFirstTime: true)
        } else {
            self.showMainTab(isFirstTime: true)
        }
        
//        let deviceLanguage = self.getLanguage()
//        print(deviceLanguage)
//        UserDefaults.standard.set(deviceLanguage, forKey: Constants.DEVICE_LANGUAGE)

//        self.mainTabBar = HomeTabBarController()
//        if let tabBarController = self.window!.rootViewController as? HomeTabBarController {
//            tabBarController.selectedIndex = 1
//        }
        
        return true
    }
    
    func showLogin(isFirstTime: Bool) {
        
        let snapShot = self.window?.snapshotView(afterScreenUpdates: true)

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let loginViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewControllerID")
        
        let mainNavigation = UINavigationController(rootViewController: loginViewController)
        
        let mainTabBar = storyBoard.instantiateViewController(withIdentifier: "mainTabBarEnID")
        
        if !isFirstTime {
            if snapShot != nil {
                
                mainTabBar.view.addSubview(snapShot!)
            }
            
            self.window?.rootViewController = mainNavigation
            UIView.animate(withDuration: 0.2, animations: {
                snapShot?.layer.opacity = 0
                snapShot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }) { (finished) in
                snapShot?.removeFromSuperview()
            }
            
            return
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = mainNavigation
        self.window?.makeKeyAndVisible()
    }
    
    func showMainTab(isFirstTime: Bool) {
        let language = UserDefaults.standard.string(forKey: Constants.DEVICE_LANGUAGE)

        let snapShot = self.window?.snapshotView(afterScreenUpdates: true)
        
        //mainTabBarEnID
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewControllerID")
        var mainTabBar = storyBoard.instantiateViewController(withIdentifier: "mainTabBarEnID")
        
        if  language != nil {
            if language == "ar" {
                mainTabBar = storyBoard.instantiateViewController(withIdentifier: "mainTabBarArID")
            }
        }
        
        if isFirstTime {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = mainTabBar
            self.window?.makeKeyAndVisible()
            return
        }
        
        if snapShot != nil {
            
            loginViewController.view.addSubview(snapShot!)
        }
        
        self.window?.rootViewController = mainTabBar
        UIView.animate(withDuration: 0.2, animations: {
            snapShot?.layer.opacity = 0
            snapShot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
        }) { (finished) in
            snapShot?.removeFromSuperview()
        }
    }
    
//    func getLanguage() -> String? {
//        var lang: String?
//
//        if  lang == nil {
//
//            let prefferedLanguage = Locale.preferredLanguages[0] as String
//            print (prefferedLanguage) //en-US
//
//            let arr = prefferedLanguage.components(separatedBy: "-")
//            let langStr  = arr.first
//
//            if langStr != "ar" || langStr == "en" {
//                return "en"
//            }
//            return langStr
//        }
//        return lang
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

