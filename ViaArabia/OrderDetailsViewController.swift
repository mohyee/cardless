//
//  OrderDetailsViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/18/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet var headerProductImage: UIImageView!
    @IBOutlet var headerProductIDLabel: UILabel!
    @IBOutlet var headerProductSubTitleLabel: UILabel!
    @IBOutlet var headerProductDateLabel: UILabel!
    @IBOutlet var searchBar: UISearchBar!

    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet weak var availableCreditLabel: UILabel!
    
    lazy var language = SharedData.getInstance.getLanguage()

    var isPresentActive: Bool = false
    var orderHistoryObject: OrderHistoryObject?
    var itemDetailsObjects = [ItemDetailsObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Order Details"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if language == "ar" {
            self.title = "تفاصيل الشراء"
            self.tableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.searchBar.placeholder = "بحث"
            
            //self.tableHeaderView.transform    = CGAffineTransform(scaleX: -1, y: 1)
            self.headerProductImage.transform = CGAffineTransform(scaleX: -1, y: 1)

            self.headerProductIDLabel.transform     = CGAffineTransform(scaleX: -1, y: 1)
            self.headerProductIDLabel.textAlignment = NSTextAlignment.right
            
            self.headerProductSubTitleLabel.transform     = CGAffineTransform(scaleX: -1, y: 1)
            self.headerProductSubTitleLabel.textAlignment = NSTextAlignment.right
            
            self.headerProductDateLabel.transform     = CGAffineTransform(scaleX: -1, y: 1)
            self.headerProductDateLabel.textAlignment = NSTextAlignment.right
        }
        
        let shareButton = UIBarButtonItem(image: UIImage(named: "shareButton"), style: .plain, target: self, action: #selector(OrderDetailsViewController.shareAction(sender:))) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = shareButton

        self.headerProductImage.layer.cornerRadius = 3.0

        let userCredit: String  = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!
        self.availableCreditLabel.attributedText =  Util.balanceSubscriptText(creditString: userCredit, subStringSize: 12.5)

        self.tableView.isHidden   = true
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "orderDetailsCell")
     
        if isPresentActive {
            let closeButton = UIBarButtonItem(image: UIImage(named: "x-blue"), style: .plain, target: self, action: #selector(OrderDetailsViewController.closeAction(sender:))) // action:#selector(Class.MethodName) for swift 3
            self.navigationItem.leftBarButtonItem  = closeButton
            
            print(self.itemDetailsObjects[0].skuName)
            let itemObj = self.itemDetailsObjects[0]
            
            self.headerProductSubTitleLabel.text = "\(self.itemDetailsObjects.count) \(itemObj.skuName)"
            self.headerProductIDLabel.text = itemObj.transactionId
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let result = formatter.string(from: date)
            self.headerProductDateLabel.text = result

            if let url = URL(string: itemObj.categoryImgUrl) {
                self.downloadImage(url: url, imageView: self.headerProductImage)
            }
            
            self.loading.isHidden = true
            self.tableView.isHidden = false
            self.tableView.reloadData()
            
        } else {
            self.orderHistoryDetailsAPICall()
        }
        
    }
    
    func orderHistoryDetailsAPICall() {
        
        let token: String    = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = ["method":   Constants.METHOD_ORDER_HISTORY_DETAILS,
               "transactionid":self.orderHistoryObject!.transactionId
        ]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_ORDER_HISTORY_DETAILS, parameters: params, headers: headers) { (data) in
            if data != nil {
                
                print(data!)
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                if statusValue == 0 {
                    
                    for temp in status["itemdet"].arrayValue {
                        let itemDetailsObject = ItemDetailsObject(itemDetails: temp)
                        self.itemDetailsObjects.append(itemDetailsObject)
                    }
                    
                    let itemObj = self.itemDetailsObjects[0]
                    
                    self.headerProductSubTitleLabel.text = "\(self.itemDetailsObjects.count) \(itemObj.skuName)"
                    self.headerProductIDLabel.text = itemObj.transactionId
                    
                    let date = itemObj.orderdate.components(separatedBy: "T")
                    self.headerProductDateLabel.text = date[0]
                    
                    if let url = URL(string: itemObj.categoryImgUrl) {
                        self.downloadImage(url: url, imageView: self.headerProductImage)
                    }
                    
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    
                } else if statusValue == 206 {
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                    })
                    return
                    
                } else {
                    
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                    })
                    return
                    
                }
                
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }

    func closeAction(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func shareAction(sender: UIBarButtonItem) {
        print("Share Action Pressed")
        
        let codeStrings:[String] = itemDetailsObjects.map { $0.code }
        var sharableCodes = String()
        let itemObj = self.itemDetailsObjects[0]
        let userName: String    = UserDefaults.standard.string(forKey: Constants.USER_NAME)!

        for item in itemDetailsObjects {
            sharableCodes.append("Pin Code:\n\(item.code)\nSerial Number:\n\(item.serial) \n\n")
        }
        
        let invoiceString = "Date: \(orderHistoryObject!.confirmDate)\nShop:\(userName)\nTransaction #:\(orderHistoryObject!.transactionId)\n-------------------------\n SKU #: \(itemObj.sku)\n SKU Description:\(itemObj.skuName) \n-------------------------\n \(sharableCodes)\n-------------------------\n www.Cardless.com.sa "
        
        print(invoiceString)

        
        let textToShare = [ invoiceString ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        //        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        self.present(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler = {(activityType, success, items, error) in
            if success {
                print("success")
            }
        }
    }
    
    // MARK: Table Delegates Implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.tableHeaderView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemDetailsObjects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsTableViewCell") as? OrderDetailsTableViewCell
        
        if cell == nil {
            let nib: [Any] = Bundle.main.loadNibNamed("OrderDetailsTableViewCell", owner: self, options: nil)!
            cell = nib[0] as? OrderDetailsTableViewCell
        }
        
        let itemObject = self.itemDetailsObjects[indexPath.row]
        
        cell!.numberLabel.text  = "\(indexPath.row + 1)"
        cell!.detailsLabel.text = "Code: \(itemObject.code)"
        
        if language == "ar" {
            cell!.numberLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.numberLabel.textAlignment = NSTextAlignment.right
            cell!.detailsLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.detailsLabel.textAlignment = NSTextAlignment.right
        }
        
        cell!.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let itemDetailsObject = self.itemDetailsObjects[indexPath.row]
        self.performSegue(withIdentifier: Constants.SEGUE_ORDER_DETAILS_PAYMENT, sender: itemDetailsObject)
    }

    //Getting Image from URL
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using
        
        if segue.identifier == Constants.SEGUE_ORDER_DETAILS_PAYMENT {
            let itemDetail =  sender as? ItemDetailsObject ?? nil
            let orderPaymentController = segue.destination as! OrderPaymentViewController
            orderPaymentController.itemDetailsObject = itemDetail
            
        }
        // Pass the selected object to the new view controller.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
