//
//  MyOrdersViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyOrdersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet weak var availableCreditLabel: UILabel!
    @IBOutlet var searchBar: UISearchBar!

    @IBOutlet var sectionHeaderView: UIView!
    @IBOutlet var sectionTitle: UILabel!
    
    var isLoading: Bool   = false
    var stopLoading: Bool = false
    
    lazy var language = SharedData.getInstance.getLanguage()
    var orderHistoryObjectsGroupByDate = [[OrderHistoryObject]]()
    var orderHistoryObjectsFiltered    = [[OrderHistoryObject]]()
    var orderHistoryObjectsOriginal    = [[OrderHistoryObject]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Orders"
        
        if language == "ar" {
            self.tableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.title = "مشترياتي"
            self.searchBar.placeholder = "بحث"
        }
        
        self.searchBar.delegate = self
        
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myOrdersCell")
        
        let userCredit: String  = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!
        self.availableCreditLabel.attributedText =  Util.balanceSubscriptText(creditString: userCredit, subStringSize: 12.5)
        
        let refreshButton = UIBarButtonItem(image: UIImage(named: "refreshIcon"), style: .plain, target: self, action: #selector(self.sync)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem = refreshButton
        
//        self.tableView.isHidden = true
        self.orderHistoryAPICall(fromDate: "", toDate: "")

    }
    
    func sync() {
        self.stopLoading = false
        self.isLoading = false
        self.orderHistoryObjectsGroupByDate = [[OrderHistoryObject]]()
        self.tableView.reloadData()
        self.orderHistoryAPICall(fromDate: "", toDate: "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
        if searchText.count > 0 {
            self.orderHistoryObjectsFiltered = self.orderHistoryObjectsGroupByDate
            var newArrData = [[OrderHistoryObject]]()
            for groupedData in self.orderHistoryObjectsFiltered {
                let newData = groupedData.filter({ $0.categoryName.contains(searchText) })
                newArrData.append(newData)
            }
            self.orderHistoryObjectsFiltered = newArrData
            self.orderHistoryObjectsGroupByDate = self.orderHistoryObjectsFiltered
            self.tableView.reloadData()
            
        } else {
            self.orderHistoryObjectsGroupByDate = [[OrderHistoryObject]]()
            self.tableView.reloadData()
            self.orderHistoryObjectsGroupByDate = self.orderHistoryObjectsOriginal
            self.tableView.reloadData()
        }
        
    }
    
    func orderHistoryAPICall(fromDate: String, toDate: String) {
        
        self.isLoading = true
        
        let token: String    = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = ["method":   Constants.METHOD_ORDER_HISTORY,
                      "fromdate": fromDate,
                      "todate": toDate]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_ORDER_HISTORY, parameters: params, headers: headers) { (data) in
            if data != nil {
                
                print(data!)
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                
                if statusValue == 0 {
                    var orderHistoryObjects = [OrderHistoryObject]()
                    
                    for temp in status["itemdet"].arrayValue {
                        
                        let orderHistoryObject = OrderHistoryObject(transaction: temp)
                        orderHistoryObject.confirmDate = self.trimDate(date: orderHistoryObject.confirmDate)
                        
                        orderHistoryObjects.append(orderHistoryObject)
                    }
                    
                    if orderHistoryObjects.count <= 0 {
                        self.stopLoading = true
                        self.isLoading   = false
                    } else {
                        orderHistoryObjects = orderHistoryObjects.reversed()
                        
                        let groupedData: [[OrderHistoryObject]] = orderHistoryObjects.groupBy{ $0.confirmDate }
                        
                        if fromDate == "" && toDate == "" {
                            self.orderHistoryObjectsGroupByDate = groupedData
                        } else {
                            for data in groupedData {
                                self.orderHistoryObjectsGroupByDate.append(data)
                            }
                        }
                        self.orderHistoryObjectsFiltered = self.orderHistoryObjectsGroupByDate
                        self.orderHistoryObjectsOriginal = self.orderHistoryObjectsGroupByDate
                        self.isLoading   = false
                    }
                    
//                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    
                } else if statusValue == 206 {
                    
                    self.stopLoading = true
                    self.isLoading   = false
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                    })
                    return
                } else {
                    
                    self.stopLoading = true
                    self.isLoading   = false
                    
                    let message = status["statusdiscription"].stringValue
                    Util.showInfoAlert(msg: message, buttonTitle: "OK", controller: self, callback: { (success) in
                    })
                    return
                }
            } else {
                self.stopLoading = true
                self.isLoading   = false
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }
    
    func trimDate(date: String)->String {
        let dateTrimmed = date.components(separatedBy: "T")
        return dateTrimmed[0]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.orderHistoryObjectsGroupByDate.count
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.orderHistoryObjectsGroupByDate[section][0].confirmDate
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: self.sectionHeaderView.frame)
        headerView.backgroundColor = self.sectionHeaderView.backgroundColor
        
        let sectionTitle = UILabel(frame: self.sectionTitle.frame)
        sectionTitle.font = self.sectionTitle.font
        sectionTitle.textColor = self.sectionTitle.textColor
        sectionTitle.text = self.orderHistoryObjectsGroupByDate[section][0].confirmDate
        
        if language == "ar" {
            sectionTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
            sectionTitle.textAlignment = NSTextAlignment.right
        }
        
        headerView.addSubview(sectionTitle)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderHistoryObjectsGroupByDate[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersTableViewCell") as? MyOrdersTableViewCell
        
        if cell == nil {
            let nib: [Any] = Bundle.main.loadNibNamed("MyOrdersTableViewCell", owner: self, options: nil)!
            cell = nib[0] as? MyOrdersTableViewCell
        }
        
        
        let orderHistoryObject = self.orderHistoryObjectsGroupByDate[indexPath.section][indexPath.row]
        
        cell!.myOrdersTitle.text = orderHistoryObject.transactionId
        cell!.myOrdersSubTitle.text = "\(Int(orderHistoryObject.qty)) \(orderHistoryObject.categoryName)"
        
        if let url = URL(string: orderHistoryObject.categoryImgUrl) {
            self.downloadImage(url: url, imageView: cell!.myOrdersImage)
        }
        
        if language == "ar" {
            cell!.myOrdersTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.myOrdersTitle.textAlignment = NSTextAlignment.right
            cell!.myOrdersSubTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.myOrdersSubTitle.textAlignment = NSTextAlignment.right
            cell!.myOrdersImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        cell!.accessoryType = .disclosureIndicator
        
        if !self.stopLoading {
            
            if self.isLoading {
                print("isLoading...")
            } else {
                
                if indexPath.section == self.orderHistoryObjectsGroupByDate.count - 1 {
                    
                    if indexPath.row == self.orderHistoryObjectsGroupByDate[indexPath.section].count - 1 {
                        
                        let sectionDate = self.orderHistoryObjectsGroupByDate[indexPath.section][0].confirmDate
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        
                        let date = formatter.date(from: sectionDate)
                        
                        let newToDate    = self.generateDates(numberOfDays: -1, date: date!)
                        let newFromDate  = self.generateDates(numberOfDays: -7, date: newToDate)
                        
                        let newToDateString   = formatter.string(from: newToDate)
                        let newFromDateString = formatter.string(from: newFromDate)
                        
                        self.orderHistoryAPICall(fromDate: newFromDateString, toDate: newToDateString)
                    }
                    
                }
            }
        }
        
        
        return cell!
    }
    
    func generateDates(numberOfDays: Int, date: Date) -> Date {
        
        let newDate    = Calendar.current.date(byAdding: .day, value: numberOfDays, to: date)

        return newDate!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let orderHistoryObject = self.orderHistoryObjectsGroupByDate[indexPath.section][indexPath.row]
        self.performSegue(withIdentifier: Constants.SEGUE_ORDER_DETAILS, sender: orderHistoryObject)
    }
    
    //Getting Image from URL
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using
        
        if segue.identifier == Constants.SEGUE_ORDER_DETAILS {
        
            let orderHistoryObject =  sender as? OrderHistoryObject ?? nil
            let orderDetailsController = segue.destination as! OrderDetailsViewController
            orderDetailsController.orderHistoryObject = orderHistoryObject
        }
        // Pass the selected object to the new view controller.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
