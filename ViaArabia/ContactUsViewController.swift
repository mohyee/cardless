//
//  ContactUsViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 9/29/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet var feelFreeLabel: UILabel!
    @IBOutlet var emailAddressLabel: UILabel!
    @IBOutlet var emailValueLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!
    @IBOutlet var websiteValueLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var phoneNumberValueLabel: UILabel!
    
    lazy var language = SharedData.getInstance.getLanguage()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        self.feelFreeLabel.text = "Please feel free to contact us at any time"
        self.emailAddressLabel.text = "EMAIL ADDRESS"
        self.emailValueLabel.text = "cardless@viaarabia.com"
        self.websiteLabel.text = "WEBSITE"
        self.websiteValueLabel.text = "www.viaarabia.com"
        self.phoneNumberLabel.text = "PHONE NUMBER"
        self.phoneNumberValueLabel.text = "920007427"
        
        if language == "ar" {
            self.title = "تواصل معنا"
            
            self.feelFreeLabel.text     = "في اي وقت يمكنكم دائماً التواصل معنا"
            self.emailAddressLabel.text = "عنوان الإيميل"
            self.websiteLabel.text      = "موقع الالكتروني"
            self.phoneNumberLabel.text  = "هاتف"
            
            self.emailAddressLabel.textAlignment     = NSTextAlignment.right
            self.websiteLabel.textAlignment          = NSTextAlignment.right
            self.phoneNumberLabel.textAlignment      = NSTextAlignment.right
            self.emailValueLabel.textAlignment       = NSTextAlignment.right
            self.websiteValueLabel.textAlignment     = NSTextAlignment.right
            self.phoneNumberValueLabel.textAlignment = NSTextAlignment.right
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
