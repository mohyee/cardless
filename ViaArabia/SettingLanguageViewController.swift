//
//  SettingLanguageViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 11/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class SettingLanguageViewController: UIViewController {

    @IBOutlet weak var settingLanguageLabel: UILabel!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    lazy var language = SharedData.getInstance.getLanguage()

    override func viewDidLoad() {
        super.viewDidLoad()

        if language != "ar" {
             self.settingLanguageLabel.text = "Setting Language..."
        } else {
            self.settingLanguageLabel.text  = "جاري تعيين اللغة..."
            self.loadingView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        Timer.scheduledTimer(timeInterval: 3,
                             target: self,
                             selector: #selector(self.dismissView),
                             userInfo: nil,
                             repeats: false)
    }

    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
