//
//  SupportViewController.swift
//  cardless
//
//  Created by Mohyee Tamimi on 9/29/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {

    @IBOutlet var feelFreeLabel: UILabel!
    @IBOutlet var emailAddressLabel: UILabel!
    @IBOutlet var emailValueLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!
    @IBOutlet var websiteValueLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var phoneNumberValueLabel: UILabel!
    @IBOutlet var callNowButton: UIButton!
    
    var emailCopied: String = "Email Copied"
    var phoneCopied: String = "Phone Copied"
    var okString   : String = "Ok"
    
    lazy var language = SharedData.getInstance.getLanguage()

    override func viewWillAppear(_ animated: Bool) {
        
        self.title = "Support"
        
        self.feelFreeLabel.text = "Please feel free to contact us at any time"
        self.emailAddressLabel.text = "EMAIL ADDRESS"
        self.emailValueLabel.text = "cardless@viaarabia.com"
        self.websiteLabel.text = "WEBSITE"
        self.websiteValueLabel.text = "www.viaarabia.com"
        self.phoneNumberLabel.text = "PHONE NUMBER"
        self.phoneNumberValueLabel.text = "920007427"
        self.callNowButton.setTitle("CALL NOW", for: UIControlState.normal)
        
        
        if language == "ar" {
            self.title = "الدعم"

            self.feelFreeLabel.text     = "في اي وقت يمكنكم دائماً التواصل معنا"
            self.emailAddressLabel.text = "عنوان الإيميل"
            self.websiteLabel.text      = "موقع الالكتروني"
            self.phoneNumberLabel.text  = "هاتف"
            self.callNowButton.setTitle("كلمنا الان", for: UIControlState.normal)
            
            self.emailCopied = "تم نسخ البريد الالكتروني"
            self.phoneCopied = "تم نسخ الرقم"
            self.okString    = "موافق"
            
            
            self.emailAddressLabel.textAlignment     = NSTextAlignment.right
            self.websiteLabel.textAlignment          = NSTextAlignment.right
            self.phoneNumberLabel.textAlignment      = NSTextAlignment.right
            self.emailValueLabel.textAlignment       = NSTextAlignment.right
            self.websiteValueLabel.textAlignment     = NSTextAlignment.right
            self.phoneNumberValueLabel.textAlignment = NSTextAlignment.right

        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func emailAction(_ sender: UIButton) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = "cardless@viaarabia.com"
        
        Util.showInfoAlert(msg: self.emailCopied, buttonTitle: self.okString, controller: self) { (success) in
            print("Success")
        }
    }
    
    @IBAction func phoneAction(_ sender: UIButton) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = "920007427"
        
        Util.showInfoAlert(msg: self.phoneCopied, buttonTitle: self.okString, controller: self) { (success) in
            print("Success")
        }
    }
    
    @IBAction func websiteAction(_ sender: UIButton) {
        
        guard let url = URL(string: "http://www.viaarabia.com") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func CallAction(_ sender: UIButton) {
        
        let phoneNumber = "920007427"
        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
