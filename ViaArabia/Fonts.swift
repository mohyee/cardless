//
//  Fonts.swift
//  AlMeter
//
//  Created by Amer Smadi on 4/26/16.
//  Copyright © 2016 Amer Smadi. All rights reserved.
//

import UIKit

open class Fonts {
    
    open static func getCairoFontBlack(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Cairo", "NSFontFaceAttribute": "Black"])
        let blackFont = UIFont(descriptor: fontDescriptor, size: size)
        return blackFont
    }
    
    open static func getCairoFontBold(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Cairo", "NSFontFaceAttribute": "Bold"])
        let boldFont = UIFont(descriptor: fontDescriptor, size: size)
        return boldFont
    }
    
    open static func getCairoFontExtraLight(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Cairo", "NSFontFaceAttribute": "ExtraLight"])
        let extraLightFont = UIFont(descriptor: fontDescriptor, size: size)
        return extraLightFont
    }
    
    open static func getCairoFontLight(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Cairo", "NSFontFaceAttribute": "Light"])
        let lightFont = UIFont(descriptor: fontDescriptor, size: size)
        return lightFont
    }
    
    open static func getHelveticaFontBold(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Helvetica Neue", "NSFontFaceAttribute": "Bold"])
        let regularFont = UIFont(descriptor: fontDescriptor, size: size)
        return regularFont
    }
    
    open static func getCairoFontSemiBold(_ size: CGFloat) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: ["NSFontFamilyAttribute": "Cairo", "NSFontFaceAttribute": "SemiBold"])
        let semiBold = UIFont(descriptor: fontDescriptor, size: size)
        return semiBold
    }
    
}
