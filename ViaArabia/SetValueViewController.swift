//
//  SetValueViewController.swift
//  ViaArabia
//
//  Created by Mohyee Tamimi on 9/16/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class SetValueViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet weak var availableCreditLabel: UILabel!

    lazy var language = SharedData.getInstance.getLanguage()
    var productObjects: [ProductObject]!
    var category = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Select Value"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        if language == "ar" {
            self.tableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.title = "اختر القيمة"
        }
        
        self.tableView.isHidden   = true
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "setValueCell")
        
        let userCredit: String  = UserDefaults.standard.string(forKey: Constants.USER_CREDIT)!
        self.availableCreditLabel.attributedText =  Util.balanceSubscriptText(creditString: userCredit, subStringSize: 12.5)

        self.productAPICall()
    }

    func productAPICall() {
        
        self.productObjects = [ProductObject]()
        let token: String    = UserDefaults.standard.string(forKey: Constants.USER_TOKEN)!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_API_USER_NAME: Constants.PARAM_API_USER_NAME_VALUE,
                        Constants.PARAM_API_PASSWORD: Constants.PARAM_API_PASSWORD_VALUE,
                        Constants.PARAM_API_REFERENCE_NUMBER: "123456",
                        Constants.PARAM_API_LANG_CODE: self.language ?? "en",
                        Constants.PARAM_API_TOKEN: token]
        
        let params = ["method":   Constants.METHOD_LIST_PRODUCT,
                      "category": category]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_LIST_PRODUCT, parameters: params, headers: headers) { (data) in
            if data != nil {
                
                print(data!)
                self.loading.stopAnimating()
                
                let jsonData    = JSON(data!)
                let results     = jsonData["result"].arrayValue
                let status      = results[0]
                let statusValue = status["status"].intValue
                
                if statusValue == 0 {
                    
                    for temp in status["productdet"].arrayValue {
                        let productObject = ProductObject(product: temp)
                        self.productObjects.append(productObject)
                    }
                    
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    
                } else if statusValue == 206 {
                    
                    Util.showInfoAlert(msg: status["statusdiscription"].stringValue, buttonTitle: "OK", controller: self, callback: { (success) in
                        
                        UserDefaults.standard.set(nil, forKey: Constants.USER_TOKEN)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showLogin(isFirstTime: false)
//                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                    })
                    return
                }
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", buttonTitle: "OK", controller: self, callback: { (success) in
                })
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productObjects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SetValueTableViewCell") as? SetValueTableViewCell
        
        if cell == nil {
            let nib: [Any] = Bundle.main.loadNibNamed("SetValueTableViewCell", owner: self, options: nil)!
            cell = nib[0] as? SetValueTableViewCell
        }
        
        let productObject = self.productObjects[indexPath.row]
        cell!.priceLabel.text = "\(productObject.skuName)"
        
        if productObject.qty != 0 {
            cell!.notAvailableView.isHidden = true
            cell!.priceLabel.frame.size.width = cell!.contentView.frame.size.width - 16
        } else {
            cell!.notAvailableView.isHidden = false
            cell!.priceLabel.textColor = UIColor.lightGray
            cell!.isUserInteractionEnabled = false
        }
        
        if language == "ar" {
            cell!.notAvailableLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.notAvailableLabel.text = "غير متوفر"
            cell!.priceLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            cell!.priceLabel.textAlignment = NSTextAlignment.right
        }

        cell!.accessoryType = .disclosureIndicator
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let productObject = self.productObjects[indexPath.row]
        self.performSegue(withIdentifier: Constants.SEGUE_PAYMENT, sender: productObject)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.SEGUE_PAYMENT {
            let productObject =  sender as? ProductObject ?? nil
            let paymentViewController = segue.destination as! PaymentViewController
            paymentViewController.productObject = productObject
        }
        // Pass the selected object to the new view controller.
    }
 

}
